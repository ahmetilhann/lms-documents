# API Rules Services of LMS

It was created to set the standards for LMS micro services.

### API request methods types

    - GET: get transaction, getting result with query
    - POST: Can be used instead of get in long queries, created object
    - PUT: create, update object
    - DELETE: delete object,

### API status codes

    2xx

    - 200: The standard HTTP response representing success for GET, PUT or POST.

    - 204 No Content: represents the request is successfully processed, but has not returned any content. 

    4xx

    - 400 Bad Request

    - 401 Unauthorized

    - 403 Forbidden

    - 404 Not Found 


    5xx

    - 500 Internal Server Error

    - 503 Service Unavailable

### API response formats

- 200

    pagination:

        {
            "data": [
                // items
            ],
            meta: {
                "pagination": {
                    "current_page": 1,
                    "per_page": 20,
                    "total_pages": 5,
                    "total": 94
                }
            }
        }

    others-1:

        {
            "data": [
                { 
                    "school_count": 7,
                    "spend_time": 0,
                    "total_student_count": 6,
                    "used_student_count": 1
                }
            ]
        }

    others-2:
    
        {
            "school_count": 7,
            "spend_time": 0,
            "total_student_count": 6,
            "used_student_count": 1
        }

- General error response

    400: form not valid:

        {
            "message": "Form verileri geçersiz.",
            "errors": [
                {
                    "type": "title",
                    "message": "Bu deger bos birakilamaz."
                }
            ],
            "data": null
        }

    others error response:

        {
            "message": "",
            "errors": null, // only form validation
            "data": null // only development
        }


